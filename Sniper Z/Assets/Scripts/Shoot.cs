using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public bool headHit = false; //If hit head execute animation of death and restart position.
    public bool bodyHit = false; //If hit body execute animation of hit.
    int layer_mask;
    public Camera cam;

    public float reloadTimer;
    float interReloadTimer;
    public int bullets;
    int bulletsInt;

    void Start()
    {
        bulletsInt = bullets;
        interReloadTimer = reloadTimer;
        layer_mask = LayerMask.GetMask("Enemy");
    }

    void Update()
    {
        if(cam.GetComponent<Breathing>().isScoping == true)
        {
            if (Input.GetButtonDown("Fire1") && bulletsInt > 0)
            {
                ShootAction();
            }

         
        }

        if(bulletsInt != bullets)
        {
            if (bulletsInt <= 0 || Input.GetKeyDown(KeyCode.R))
            {
                interReloadTimer -= Time.deltaTime;
            }
        }
        

        if(interReloadTimer <= 0)
        {
            interReloadTimer = reloadTimer;
            bulletsInt = bullets;
        }
        //Debug.Log(bulletsInt);
    }

    void ShootAction()
    {
        RaycastHit hit;

        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 100000, layer_mask))
        {
            //Debug.Log(hit.transform.name);

            if (hit.transform.name == "Body")
            {
                //hit.transform.gameObject.GetComponent<AI>().animator.SetBool("Move", true);
                //Debug.LogError(hit.transform.name);
            }
            else if (hit.transform.name == "Head")
            {
                //Debug.LogError(hit.transform.name);
                hit.transform.gameObject.GetComponentInParent<AI>().die = true;
            }

            //if (hit.transform.gameObject.CompareTag("Enemy"))
            //{
            //     if(hit.transform.name == "Body")
            //     {
            //         //hit.transform.gameObject.GetComponent<AI>().animator.SetBool("Move", true);
            //         Debug.LogError(hit.transform.name);
            //     }
            //     else if(hit.transform.name == "Head")
            //     {
            //         Debug.LogError(hit.transform.name);
            //         hit.transform.gameObject.GetComponent<AI>().die = true;
            //     }
            //}
        }

        bulletsInt--;
    }
}
