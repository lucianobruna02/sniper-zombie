using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoped : MonoBehaviour
{
    public Animator animator;

    public bool isScoped;

    public bool animFinish;

    public GameObject scopeOverlay;

    public GameObject gunCamera;

    public Camera mainCamera;

    public float scopedFOV = 15f;

    private float normalFOV;

    private void Start()
    {
        isScoped = false;
        normalFOV = mainCamera.fieldOfView;
        animFinish = true;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire2") && animFinish == true)
        {
            isScoped = !isScoped;

            animFinish = false;

            animator.SetBool("Scoped", isScoped);

            if (isScoped)
                StartCoroutine(OnScoped());
            else
                OnUnscoped();
        }


    }
    void OnUnscoped()
    {
        scopeOverlay.SetActive(false);
        gunCamera.SetActive(true);

        mainCamera.fieldOfView = normalFOV;

        animFinish = true;
    }

    IEnumerator OnScoped()
    {
        yield return new WaitForSeconds(.15f);

        scopeOverlay.SetActive(true);
        gunCamera.SetActive(false);

        
        mainCamera.fieldOfView = scopedFOV;

        animFinish = true;
    }

 
}
