using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breathing : MonoBehaviour
{
    public bool breathY = true;
    public bool breathX = true;

    public bool isScoping = false;

    public GameObject gun;

    public float minYHigh = -0.2f;
    public float maxYHigh = 0.2f;
    public float minXHigh = -0.1f;
    public float maxXHigh = 0.1f;

    public float movementY;
    public float movementX;

    public bool holdingBreath;
    public bool canHoldBreath;

    [Range (0f,1f)]
    public float holdBreath;

    public float breathTimer;
    float InterBreathTimer;

    void Start()
    {
        canHoldBreath = true;
        holdingBreath = false;
        holdBreath = 1;
        InterBreathTimer = breathTimer;
    }


    void Update()
    {

        //if (Input.GetButtonDown("Fire2"))
        //{
        //    isScoping = !isScoping;
        //}

        if (gun.gameObject.GetComponent<Scoped>().mainCamera.fieldOfView == gun.gameObject.GetComponent<Scoped>().scopedFOV)
            isScoping = true;
        else
            isScoping = false;

        if (isScoping == true)
        {
            if (breathY)
            {
                #region yUP
                movementY = Mathf.Lerp(movementY, maxYHigh, Time.deltaTime * 3f * holdBreath);

                transform.localPosition = new Vector3(transform.localPosition.x, movementY, transform.localPosition.z);

                if (movementY >= maxYHigh - 0.01f)
                {
                    breathY = !breathY;
                }
                #endregion


            }

            else
            {
                #region yDown
                movementY = Mathf.Lerp(movementY, minYHigh, Time.deltaTime * 3f * holdBreath);
                transform.localPosition = new Vector3(transform.localPosition.x, movementY, transform.localPosition.z);

                if (movementY <= minYHigh + 0.01f)
                    breathY = !breathY;
                #endregion

            }

            if (breathX)
            {
                #region xUP
                movementX = Mathf.Lerp(movementX, maxXHigh, Time.deltaTime * 3f * holdBreath);

                transform.localPosition = new Vector3(movementX, transform.localPosition.y, transform.localPosition.z);

                if (movementX >= maxXHigh - 0.01f)
                {
                    breathX = !breathX;
                }
                #endregion


            }

            else
            {
                #region xDown
                movementX = Mathf.Lerp(movementX, minXHigh, Time.deltaTime * 3f * holdBreath);
                transform.localPosition = new Vector3(movementX, transform.localPosition.y, transform.localPosition.z);

                if (movementX <= minXHigh + 0.01f)
                    breathX = !breathX;
                #endregion

            }


            if(holdBreath >= 0.9f && InterBreathTimer >= 0.9f)
            {
                canHoldBreath = true;
            }
            else if(holdBreath <= 0.1f)
            {
                canHoldBreath = false;
            }

            if(Input.GetKey(KeyCode.Space) && canHoldBreath == true)
            {
                holdingBreath = true;
            }
            else if(Input.GetKeyUp(KeyCode.Space) || canHoldBreath == false)
            {
                holdingBreath = false;
            }

            if(holdingBreath == true)
            {
                holdBreath = Mathf.Lerp(holdBreath, 0.05f, Time.deltaTime * 1);
                if (holdBreath <= 0.1f)
                {
                    holdingBreath = false;
                    InterBreathTimer -= Time.deltaTime;
                }
                   
            }

            else if(holdingBreath == false)
            {
                holdBreath = Mathf.Lerp(holdBreath, 0.95f, Time.deltaTime * 1);
                if (holdBreath >= 0.9f && InterBreathTimer <= 0.1f)
                    InterBreathTimer = breathTimer;
            }

            //if (Input.GetKey(KeyCode.Space) && holdBreath > 0.5 && InterBreathTimer > 0.5)
            //{
            //    holdingBreath = true;
            //}           

            //if (holdingBreath == true)
            //{
            //    holdBreath = Mathf.Lerp(holdBreath, 0f, Time.deltaTime * 1);
            //    if (holdBreath <= 0.5f)
            //        InterBreathTimer -= Time.deltaTime;
            //}

            //else if (holdingBreath == false)
            //{
            //    holdBreath = Mathf.Lerp(holdBreath, 1f, Time.deltaTime * 1);
            //    if(holdBreath >= 0.9f)
            //        InterBreathTimer = breathTimer;

            //}

            //if (InterBreathTimer <= 0.5f || Input.GetKeyUp(KeyCode.Space))
            //{
            //    holdingBreath = false;
            //}
        }

        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }

       
    }
    
}
