using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AI : MonoBehaviour
{
    #region Comportamiento
    public NavMeshAgent agent;

    public bool objectiveInAttack;
    public bool attackAB; //true is A, false is B
    public bool die;

    public GameObject objectiveA;
    public GameObject objectiveB;
    public Transform Finalobjective;

    public float radio;
    #endregion

    public GameObject respawn;
    float interRespawnTimer;
    public float respawnTimer;

    #region Animacion

    public int animNum;

    public Animator animator;
    public GameObject zombie;

    #endregion
    private void Awake()
    {
        objectiveA = GameObject.Find("ObjectiveA");
        objectiveB = GameObject.Find("ObjectiveB");
        interRespawnTimer = respawnTimer;
        agent = GetComponent<NavMeshAgent>();
        respawn = GameObject.Find("ZombieRespawn");
    }

    void Start()
    {
        animNum = Random.Range(0, 3);
        objectiveInAttack = false;
        die = false;
    }

    void Update()
    {
        #region Set Objective
        if (attackAB == true)
            Finalobjective = objectiveA.transform;
        else if (attackAB == false)
            Finalobjective = objectiveB.transform;
        #endregion

        if(die == false)
        {
          

            if (objectiveInAttack == false)
            {
                agent.SetDestination(Finalobjective.transform.position);


                if (animNum <= 1)
                {
                    animator.SetBool("Move", true);
                    animator.SetBool("Move 1", false);
                    animator.SetBool("Attack", false);
                }
                else if (animNum >= 2)
                {
                    animator.SetBool("Move 1", true);
                    animator.SetBool("Move", false);
                    animator.SetBool("Attack", false);
                }
            }

            else if (objectiveInAttack == true)
            {
                transform.LookAt(new Vector3(Finalobjective.position.x, transform.position.y, Finalobjective.position.z));
                animator.SetBool("Attack", true);
                animator.SetBool("Move", false);
                animator.SetBool("Move 1", false);
                agent.SetDestination(this.transform.position);
            }
        }
        else if (die == true)
        {
            animator.SetBool("Attack", false);
            animator.SetBool("Move", false);
            animator.SetBool("Move 1", false);
            objectiveInAttack = false;
            agent.SetDestination(this.transform.position);
            if (attackAB == false)
                animator.SetBool("DieForward", true);
            else if(attackAB == true)
                animator.SetBool("DieBack", true);

            interRespawnTimer -= Time.deltaTime;
            
        }
       
        if(interRespawnTimer <= 0)
        {
            this.transform.position = respawn.transform.position;
            die = false;
            animator.SetBool("DieForward", false);
            animator.SetBool("DieBack", false);
            interRespawnTimer = respawnTimer;
        }

    }



    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.transform == objectiveA.transform || other.gameObject.transform == objectiveB.transform)
        {
            objectiveInAttack = true;
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.transform == objectiveA.transform || other.gameObject.transform == objectiveB.transform)
        {
            objectiveInAttack = false;
        }

    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, radio);
    }
}
